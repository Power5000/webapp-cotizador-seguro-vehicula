let deferredPrompt;
var div    = document.querySelector('.c-pwa__wrapper');
var button = document.querySelector('.add-to-btn');

div.style.display = 'none';

window.addEventListener('beforeinstallprompt', (e) => {
    e.preventDefault();
    deferredPrompt = e;
    div.style.display = 'block';

    setTimeout(() => {
      div.style.display = 'none';
    }, 5000)

    button.addEventListener('click', (e) => {
        div.style.display = 'none';
        
        deferredPrompt.prompt();

        deferredPrompt.userChoice
        .then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
            } else {
                console.log('User dismissed the A2HS prompt');
            }
            deferredPrompt = null;
        });
    });
});