import React, { CSSProperties } from 'react';
import './Footer.sass';

type FooterProps = {
    style?: CSSProperties,
    className?: string
}

export const Footer: React.FC<FooterProps> = (
    props: FooterProps
): JSX.Element => {

    return (
        <footer className={"c-footer__content "+ props.className} style={props.style}>
            <span>© 2020 RIMAC Seguros y Reaseguros.</span>
        </footer>
    );
}