type LoadingProps = {
    title?: string,
}

const LoadingDefault = {
    title: 'Cargando...'
}

export type Props = LoadingProps;
export const Default = LoadingDefault;