import React, { CSSProperties } from 'react';
import { Icon } from '@components/common/Icon';
import { LogoRimac, Telephone } from '@constants/icons';
import { APP_TELEPHONE } from '@defaults/app';

import './Navbar.sass';

type NavbarProps = {
    style?: CSSProperties,
    className?: string
}

export const Navbar: React.FC<NavbarProps> = (
    props: NavbarProps
): JSX.Element => {

    return (
        <nav className={"c-navbar__content "+props.className} style={props.style}>
            <Icon Svg={LogoRimac} className="c-navbar__logo-rimac"/>
            <section className="c-navbar__call-user">

                <div className="c-navbar__ask --desktop">
                    ¿Tienes alguna duda?
                </div>
                <a href={"tel:"+APP_TELEPHONE} className="c-navbar__call">
                    <Icon Svg={Telephone} className="c-navbar__telephone"/>
                    <span className="c-navbar__call-text --mobile"> Llámanos </span>
                    <span className="c-navbar__call-text --desktop"> {APP_TELEPHONE} </span>
                </a>
            </section>
        </nav>
    );
}