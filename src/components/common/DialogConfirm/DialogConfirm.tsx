import { memo } from "react";
import { Props } from "./DialogConfirm.type";
import { ButtonComponent } from "../Button/Button";
import { Dialog, DialogActions, DialogContent, DialogTitle } from "@material-ui/core";

import './DialogConfirm.sass'

const ConfirmDialogComponent: React.FC<Props> = ({ open, title, message, onConfirm, onClose }) => {

   return (
      <Dialog
         open    = {open}
         onClose = {onClose}
         classes={
             {paper: "c-dialog-confirm-continer"}
         }
         BackdropProps={{ className: 'c-dialog-confirm-backdrop' }}
      >
         <DialogTitle
            className='c-dialog-confirm-title'
            disableTypography
         >
             { (title) ?  title: ''}
         </DialogTitle>
         <DialogContent className=''>
            <div className='c-dialog-confirm-message'>
               <p>{message}</p>
            </div>
         </DialogContent>
         <DialogActions className='c-dialog-confirm-action'>
            <ButtonComponent  text="Aceptar" className="outlined-small" action={onConfirm} ></ButtonComponent>
         </DialogActions>
      </Dialog>
   );
}


export const ConfirmDialog = memo(ConfirmDialogComponent);
