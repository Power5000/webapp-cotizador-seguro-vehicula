type DialogConfirmProps = {
   open: boolean,
   title?: string | React.ReactNode,
   message: string,
   onConfirm: () => void,
   onClose: () => void
}

export type Props = DialogConfirmProps;
