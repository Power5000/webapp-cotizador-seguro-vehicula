import React from 'react';
import Stepper, { MobileStepperProps } from '@material-ui/core/MobileStepper';


export const MobileStepper: React.FC<MobileStepperProps> = (
    props : MobileStepperProps
) : JSX.Element => {
    return (
        <Stepper {...props}/>
    );
}