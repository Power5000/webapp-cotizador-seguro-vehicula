import React from 'react';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';

export const InputText: React.FC<TextFieldProps> = (
    props : TextFieldProps
) : JSX.Element => {
    return (
        <TextField {...props} innerRef={props.ref}/>
    );
}