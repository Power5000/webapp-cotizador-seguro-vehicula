import React from 'react';
import { useCounter } from '@toolbox/hooks/counter.hook';

import { Props } from './Cotizate.type';
import './Cotizate.sass';

export const Cotizate: React.FC<Props> = (
    {count, min, max, increment: inc, simbol}
): JSX.Element => {

    const { counter, increment, decrement } = useCounter(count, inc);

    const handleIncrement = () => {
        if (counter < max) {
            increment()
        }
    }

    const handleDecrement = () => {
        if (counter > min) {
            decrement()
        }
    }

    return (
        <>
            <div className="c-cotizate">
                <button onClick={handleDecrement}>-</button>
                <h1>{simbol} {counter}</h1>
                <button onClick={handleIncrement}>+</button>
            </div>
        </>
    );
}