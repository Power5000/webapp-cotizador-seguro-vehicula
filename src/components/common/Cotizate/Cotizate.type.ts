type CotizateProps = {
    count: number,
    max: number,
    min: number,
    increment: number,
    simbol: string
}

export type Props = CotizateProps;