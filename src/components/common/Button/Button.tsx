import React, { SVGAttributes, memo, CSSProperties } from 'react';
import './Button.sass';

type BurronProps = {
    text: string | React.ReactNode,
    type?: 'button' | 'submit',
    className: string,
    action: () => void,
    style?: CSSProperties,
    Svg?: React.FC<SVGAttributes<any>>
}

export const ButtonComponent : React.FC<BurronProps> = (
    props: BurronProps
) : JSX.Element => {

    return (
        <button
            className={
                (props.className == 'outlined-small') ? 'c-button-outlined-small'
                    : (props.className == 'outlined-large') ? 'c-button-outlined-large'
                        : (props.className == 'contained') ?'c-button-contained'
                            : 'c-button-contained-action'
            }
            style={props.style}
            type={props.type}
            onClick={props.action}> {props.text} {props.Svg && <i className="c-button-icon">{props.Svg}</i>}</button>
    );
}

ButtonComponent.defaultProps = {
    type : 'button'
}
