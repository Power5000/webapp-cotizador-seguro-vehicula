import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MuiCheckbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import MuiRadio, { RadioProps } from '@material-ui/core/Radio';

interface CheckProps extends CheckboxProps {
    checkName: string,
    label?: string,
    onChangeCheck: (data : { [key: string] : boolean}) => void
}

interface RadProps extends RadioProps {
    checkName: string,
    label?: string,
    onChangeCheck: (key: any) => void
}


const GreenCheckbox = withStyles({
  root: {
    color: '#43B748',
    '&$checked': {
      color: '#43B748',
    },
  },
  checked: {},
})((props: CheckboxProps) => <MuiCheckbox color="default" {...props} />);

const GreenRadio = withStyles({
    root: {
        color: '#43B748',
        '&$checked': {
            color: '#43B748',
        },
    },
    checked: {},
})((props: RadioProps) => <MuiRadio color="default" {...props} />);

export function Checkbox(props: CheckProps) {
    const [state, setState] = React.useState({
        [props.checkName]: true,
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({ ...state, [event.target.name]: event.target.checked });
        props.onChangeCheck({ [event.target.name]: event.target.checked })
    };

    return (
        <FormControlLabel
            control={
                <GreenCheckbox
                    checked={state[props.checkName]}
                    onChange={handleChange} 
                    name={props.checkName}
                />
            }
            label={props.label}
        />
    );
}

export function Radio(props: RadProps) {

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        props.onChangeCheck(event.target.value);
    };

    return (
        <FormControlLabel
            control={
                <GreenRadio
                    checked={props.checked === props.value}
                    onChange={handleChange} 
                    name={props.checkName}
                    value={props.value}
                />
            }
            label={props.label}
        />
    );
}