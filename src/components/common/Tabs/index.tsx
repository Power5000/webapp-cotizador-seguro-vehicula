import React from 'react';
// import SwipeableViews from 'react-swipeable-views';
import { makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import './Tabs.sass'

interface TabPanelProps {
  children?: React.ReactNode;
  dir?: string;
  index: any;
  value: any;
}

export function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: 'white',
    width: '100%',
  },
}));

type TabProps = {
    tabLabelOne: string,
    tabLabelTwo: string,
    tabLabelThree: string,

    PanelOne: JSX.Element,
    PanelTwo: JSX.Element,
    PanelThree: JSX.Element
}

export default function FullWidthTabs(props: TabProps) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="secondary"
          variant="fullWidth"
          aria-label="full width tabs example"
          style={{
            background: 'white',
            boxShadow: 'none'
          }}
        >
          <Tab label={props.tabLabelOne} {...a11yProps(0)} />
          <Tab label={props.tabLabelTwo} {...a11yProps(1)} />
          <Tab label={props.tabLabelThree} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <div
        // onClick={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
            {props.PanelOne}
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
            {props.PanelTwo}
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
            {props.PanelThree}
        </TabPanel>
      </div>
    </div>
  );
}