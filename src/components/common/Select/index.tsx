import React from 'react';
import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

type SelectProps = {
    id?: string,
    variant?: "outlined" | 'filled',
    options: any,
    getOptionLabel: (option: any) => string,
    placeholder?: string,
    label?: string,
    style?: React.CSSProperties,
    onChange?: (e:any, option: any) => void,
    value?: any,
    error: boolean | undefined,
    disableClearable?: boolean,
    helperText: React.ReactNode
}

export const Select: React.FC<SelectProps> = (
    props: SelectProps
) : JSX.Element => {
    return (
        <Autocomplete
            id={props.id}
            options={props.options}
            getOptionLabel={props.getOptionLabel}
            style={props.style}
            onChange={props.onChange}
            value={props.value}
            disableClearable={props.disableClearable}
            renderInput={(params) =>
                <TextField {...params}
                    label={props.label}
                    variant={props.variant}
                    error={props.error}
                    helperText={props.helperText}
                    placeholder={props.placeholder}/>
            }
            renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip
                    variant="outlined"
                    label={option.title}
                    size="small"
                    {...getTagProps({ index })}
                  />
                ))
            }
        /> 
    );
}