import React from 'react';

type TitleProps = {
    text: string,
    main: string,
    reverse?: boolean
}

export const RimacTitle: React.FC<TitleProps> = (
    {text, main, reverse}
): JSX.Element => {
    return (
        <h3 className="p-title__secure">
            <span>{ reverse ? main : text}</span>
            <span>{ reverse ? text : main}</span>
        </h3>
    );
}

RimacTitle.defaultProps = { reverse: false }