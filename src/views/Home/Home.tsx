import React, { useState } from 'react';
import { Helmet } from "react-helmet";
import { Navbar } from '@components/common/Navbar';
import { Footer } from '@components/common/Footer';
import { Aside } from '@features/Aside';
import { VehicleData } from '@features/VehicleData';
import { BuildPlan } from '@features/BuildPlan';
import { ROUTE_PASS_ONE } from '@/toolbox/constants/route-map';
import { 
    APP_NAME_PROJECT,
    APP_DESCRIPTION_PROJECT
} from '@defaults/app';

import { Props } from './Home.type';
import './Home.sass';

export const HomeView : React.FC<Props> = (
    props
) : JSX.Element => {

    const [nroCurrent] = useState<'1'|'2'>(props.stepCurrent === ROUTE_PASS_ONE ? '1' : '2');

    return (
        <>
            <Helmet>
                <title>{`${APP_NAME_PROJECT} - ${APP_DESCRIPTION_PROJECT} :: ${props.title}`}</title>
            </Helmet>
            <Navbar className="p-home__navbar"/>
            <div className="p-home">
                <Aside nro={nroCurrent}/>
                <div className="p-home__slide">
                    { nroCurrent === '1' ?
                        (<VehicleData user={props.userData}/>) : 
                        (<BuildPlan user={props.userData}/>)
                    }
                </div>
            </div>
            <Footer className="p_home__footer"/>
        </>
    );
};