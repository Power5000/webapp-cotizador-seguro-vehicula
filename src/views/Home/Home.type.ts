import { UserAgreement } from '@models/User'

type HomeProps = {
    userData: UserAgreement,
    stepCurrent: string,
    title: string
}

export type Props = HomeProps;
