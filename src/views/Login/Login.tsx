import React from "react";
import { Helmet } from "react-helmet";
import { Navbar } from '@components/common/Navbar';
import { Footer } from '@components/common/Footer';
import { 
    APP_NAME_PROJECT,
    APP_DESCRIPTION_PROJECT
} from '@defaults/app';
import { LoginFeature } from '@features/Login';

import "./Login.sass";

export const LoginView: React.FC = () : JSX.Element => {
    return (
        <>
            <Helmet>
                <title>{`${APP_NAME_PROJECT} - ${APP_DESCRIPTION_PROJECT} :: Inicio`}</title>
            </Helmet>
            <Navbar style={{ background: 'transparent' }}/>
            <LoginFeature />
            <Footer/>
        </>
    );
}

