import React from 'react';
import { Helmet } from "react-helmet";
import { Navbar } from '@components/common/Navbar';
import { Greeting } from '@features/Greeting';
import { 
    APP_NAME_PROJECT,
    APP_DESCRIPTION_PROJECT
} from '@defaults/app';

import { Props } from './Greeting.type';
import './Greeting.sass';

export const GreetingView : React.FC<Props> = (
    props
) : JSX.Element => {

    return (
        <>
            <Helmet>
                <title>{`${APP_NAME_PROJECT} - ${APP_DESCRIPTION_PROJECT} :: ${props.title}`}</title>
            </Helmet>
            <Navbar className="p-greeting__navbar"/>
            <div className="p-greeting">
                <Greeting user={props.userData}/>
            </div>
        </>
    );
};