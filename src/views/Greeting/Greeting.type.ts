import { UserAgreement } from "@/service/models/User"

type Greeting = {
    title: string,
    userData: UserAgreement,
}
export type Props = Greeting