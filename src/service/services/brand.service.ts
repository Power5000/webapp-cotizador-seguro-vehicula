import { brandRepository } from "../repositories/brand.repository";

export const brandService = {
  getBrands: () => {
    return brandRepository.getBrands()
  }
};