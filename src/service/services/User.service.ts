import { userRepository } from "../repositories/user.repository";

export const userService = {
  getUser: (mail: string, zipCode: string, plate: string) => {
    return userRepository.getUser(mail,zipCode,plate)
  }
};