const headers = {
    'Content-Type': 'application/json'
  }
  
  const get = async <T>(url: string): Promise<T> => {
    const response = await fetch(url, {
      method: 'GET',
      headers
    })
    const rpta: T = await response.json()
    return rpta
  }
  
  const post = async <T>(url: string, body: any) => {
    const response = await fetch(url, {
      method: 'POST',
      headers,
      body
    })
    const rpta: T = await response.json()
    return rpta
  }
  
  const put = async <T>(url: string, body: any) => {
    const response = await fetch(url, {
      method: 'PUT',
      headers,
      body
    })
    const rpta: T = await response.json()
    return rpta
  }
  
  const _delete = async <T>(url: string) => {
    const response = await fetch(url, {
      method: 'DELETE',
      headers
    })
    const rpta: T = await response.json()
    return rpta
  }
  
  export const http = {
    get,
    post,
    put,
    delete: _delete
  }