export type BrandId = string

export interface BrandAgreement {
    name: string,
    value: string,
    catchPhrase: string,
    bs: string
}