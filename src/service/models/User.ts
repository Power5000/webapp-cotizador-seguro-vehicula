export type UserId = string

export interface Vehicle {
    name?: string,
    plateNumber?: string,
    year?: string,
    brand?: string
}

export interface UserAgreement {
    user_id: number,
    name: string,
    username: string,
    email: string,
    phone: string,
    vehicle: Vehicle
}