import { http } from './../http/http';
import { API_URL_BASE } from '@defaults/app';
import { UserDTO } from '../http/dto/UserDTO';
import { UserAgreement } from '../models/User';

export const userRepository = {
  getUser: async (mail : string , zipcode: string, plate: string) : Promise<UserAgreement | null> => {
    const users = await http.get<UserDTO[]>(`${API_URL_BASE}users?phone=${mail}&zipcode=${zipcode}`)
    const user  = users[0]
    if (!users[0]) {
      return null;
    }
    return {
      user_id: user.id,
      name: user.name,
      username: user.username,
      email: user.email,
      phone: user.phone,
      vehicle: {
        plateNumber: plate
      }
    }
  }
}