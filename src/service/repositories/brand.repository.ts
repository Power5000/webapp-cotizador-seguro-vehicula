import { http } from '../http/http';
import { API_URL_BASE } from '@defaults/app';
import { BrandDTO } from '../http/dto/BrandsDTO';
import { BrandAgreement } from '../models/Brand';

export const brandRepository = {
  getBrands: async () : Promise<BrandAgreement[]> => {
    const comp = await http.get<BrandDTO[]>(`${API_URL_BASE}users`)
    return comp.map((item) => ({
        name: item.company.name,
        value: item.company.name,
        catchPhrase: item.company.catchPhrase,
        bs: item.company.bs
    }))
  }
}