export const KEY_LOCAL_STORAGE  = process.env.REACT_APP_KEY_COOKIE || '_';
export const KEY_ARRAY_MY_MENU  = 'arrayMyMenu';
export const KEY_OBJECT_MY_MENU = 'objectMyMenu';
/*
Keys for UserTheme Preference
*/
export const KEY_USER_THEME     = 'userTheme';
export const KEY_USER_DATA      = 'userData';