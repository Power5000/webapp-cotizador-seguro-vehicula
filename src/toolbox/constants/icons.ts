import {ReactComponent as LogoMdIcon} from "@assets/svg/ic_md_login.svg";
import {ReactComponent as LogoXlIcon} from "@assets/svg/ic_xl_login.svg";
import {ReactComponent as LogoRimac} from "@assets/svg/ic_logo.svg";
import {ReactComponent as Telephone} from "@assets/svg/telephone.svg";
import {ReactComponent as BackgroundLogin} from "@assets/svg/background_login.svg";
import {ReactComponent as ArrowBack} from "@assets/svg/arrow-back.svg";
import {ReactComponent as CarIcon} from "@assets/svg/ic_car.svg";
import {ReactComponent as GreetingMdIcon} from "@assets/svg/ic_md_greeting.svg";
import {ReactComponent as GreetingXlIcon} from "@assets/svg/ic_xl_greeting.svg";
import {ReactComponent as PersonBuildPlan} from "@assets/svg/person_build_plan.svg";
import {ReactComponent as PerdidaTotal} from "@assets/svg/icon_perdidatotal.svg";
import {ReactComponent as Choque} from "@assets/svg/icon_choque.svg";
import {ReactComponent as LLantaRobada} from "@assets/svg/icon_llanta_robada.svg";
import {ReactComponent as ArrowLess} from "@assets/svg/arrow-less.svg";
import {ReactComponent as ArrowMore} from "@assets/svg/arrow-more.svg";
import {ReactComponent as TrofeoIcon} from "@assets/svg/ic_trofeo.svg";
import {ReactComponent as CheckIcon} from "@assets/svg/ic_check.svg";

export {
    LogoMdIcon,
    LogoXlIcon,
    LogoRimac,
    Telephone,
    BackgroundLogin,
    ArrowBack,
    CarIcon,
    GreetingMdIcon,
    GreetingXlIcon,
    PersonBuildPlan,
    PerdidaTotal,
    Choque,
    LLantaRobada,
    ArrowLess,
    ArrowMore,
    TrofeoIcon,
    CheckIcon
}