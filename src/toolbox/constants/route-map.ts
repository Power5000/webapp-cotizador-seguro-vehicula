export const ROUTE_HOME = '/';

export const ROUTE_PASS_ONE = '/datos-auto';
export const ROUTE_PASS_TWO = '/armar-plan';

export const ROUTE_CONGRATS = '/gracias';