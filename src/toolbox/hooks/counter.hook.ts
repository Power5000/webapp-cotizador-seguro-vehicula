import React from 'react';

export function useCounter(count, inc: number) {
  const [counter, setCounter] = React.useState(count || 0);
  const increment = () => setCounter(counter + inc);
  const decrement = () => setCounter(counter - inc);

  return { counter, increment, decrement };
}