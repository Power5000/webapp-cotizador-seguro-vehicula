export const APP_NAME_PROJECT = 'SVR';
export const APP_DESCRIPTION_PROJECT = 'Seguro Vehicular Rimac';
export const APP_TELEPHONE = '(01) 411 6001';
export const APP_DESKTOP_WIDTH = 992;

export const APP_URL_POLITICE = process.env.REACT_APP_WEB_URL_POLITICA_SEGURIDAD;
export const APP_URL_TERMS_CONDITIONS = process.env.REACT_APP_WEB_URL_TERMINO_Y_CONDICIONES;
export const APP_URL_HOW_USE_SECURE = process.env.REACT_APP_API_URL_COMO_USAR_SEGURO;
export const APP_URL_MORE_MODELS = process.env.REACT_APP_WEB_URL_MAS_MODELOS;
export const API_URL_BASE = process.env.REACT_APP_API_URL; 


export const SHOW_YEARS = 50;
export const MIN_COTIZE = 12500;
export const MAX_COTIZE = 15000;
export const INC_COTIZE = 100;