export const STATIC_ROUTERS = [
    {
      "name": "Evaluaciones",
      "icon": "EvaluateIcon",
      "moduloId": 1,
      "route": "evaluaciones",
      "isMenu": true,
      "subMenu": [
        {
            "name": "Evaluaciones",
            "icon": "EvaluateIcon",
            "page": {
              "compare": null,
              "displayName": "Connect(Component)"
            },
            "isMenu": true,
            "route": "/evaluacion-de-desempeno-organizacional",
            "actions": {
              "actionInsert": true,
              "actionUpdate": true,
              "actionDelete": true,
              "actionSearch": true
            }
          }
      ]
    },
    {
      "name": "Usuarios",
      "icon": "UserIcon",
      "moduloId": 2,
      "route": "guias",
      "isMenu": true,
      "subMenu": [
        {
          "name": "Usuarios",
          "icon": "UserIcon",
          "page": {
            "compare": null,
            "displayName": "Connect(Component)"
          },
          "isMenu": true,
          "route": "/usuarios",
          "actions": {
            "actionInsert": true,
            "actionUpdate": true,
            "actionDelete": true,
            "actionSearch": true
          }
        }
      ]
    },
    {
      "name": "Configuración",
      "icon": "SettingIcon",
      "moduloId": 3,
      "route": "guias",
      "isMenu": true,
      "subMenu": [
        {
          "name": "Configuración",
          "icon": "SettingIcon",
          "page": {
            "compare": null,
            "displayName": "Connect(Component)"
          },
          "isMenu": true,
          "route": "/configuraciones",
          "actions": {
            "actionInsert": true,
            "actionUpdate": true,
            "actionDelete": true,
            "actionSearch": true
          }
        }
      ]
    },
    {
      "name": "Vistas Usuario",
      "icon": "ViewUserIcon",
      "moduloId": 4,
      "route": "mantenimiento",
      "isMenu": true,
      "subMenu": [
        {
          "name": "Vistas Usuario",
          "icon": "ViewUserIcon",
          "page": {
            "compare": null,
            "displayName": "Connect(Component)"
          },
          "isMenu": true,
          "route": "/vistas-usuario",
          "actions": {
            "actionInsert": true,
            "actionUpdate": true,
            "actionDelete": true,
            "actionSearch": true
          }
        }
      ]
    },
    {
      "name": "CONFIGURACIONES",
      "icon": "SvgSettings",
      "moduloId": 5,
      "route": "configuraciones",
      "isMenu": true,
      "subMenu": [
        {
          "name": "ADMINISTRACION DE PERFILES",
          "icon": "SvgUserSetting",
          "page": {
            "compare": null,
            "displayName": "Connect(Component)"
          },
          "isMenu": true,
          "route": "/configuraciones/administracion-de-perfiles",
          "actions": {
            "actionInsert": true,
            "actionUpdate": true,
            "actionDelete": true,
            "actionSearch": true
          }
        },
        {
          "name": "REGISTRO DE USUARIOS",
          "icon": "SvgUserSetting",
          "page": {
            "compare": null,
            "displayName": "Connect(Component)"
          },
          "isMenu": true,
          "route": "/configuraciones/registro-de-usuario",
          "actions": {
            "actionInsert": true,
            "actionUpdate": true,
            "actionDelete": true,
            "actionSearch": true
          }
        }
      ]
    }
  ]