export function isEmpty(value) {
    if(value === null || value === undefined || value === '') {
        return true;
    }
    if(Array.isArray(value)) {
        if(value.length === 0) { return true; }
    }
    if( typeof (value) === 'object') {
        if(Object.keys(value).length === 0) { return true; }
    }
    return false;
}

export function isRequired(value) {
    if(!isEmpty(value)) {
        return true;
    }
    return false;
}
export function isNumber(num) {
    if (!isEmpty(num)) {
        const pattern = /^[0-9]+$/g;
        return pattern.test(num.toString());
    }
    return false;
}
export function isFormatPhone(text) {
    if (!isEmpty(text)) {
        const pattern = /^(\+{1}|\(|\)|[0-9]|\-|\s)+$/gim;
        return (text.toString().replace(pattern, "") === "");
    }
    return false;
}
export function isContainsCharAt(value, letter) {
    return value.toUpperCase() === letter.toUpperCase();
}
export function isContainsMail(text) {
    const pattern = /[-+\w.]{1,64}|\@|(?:[A-Z0-9-]{1,63}\.){1,125}|[A-Z]{2,63}$/gi;
    return (text.toString().replace(pattern, "") === "");
}
export function isMail(text) {
    const pattern = /^[-+\w.]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    return pattern.test(text);
}
export function isDNI(text) {
    const pattern = /^[0-9]{8,8}$/;
    return pattern.test(text);
}
export function isWebAddress(text) {
    const pattern = /[^'A-zñáéíóú ÑÁÉÍÓÚ|-|0-9|,|@|.|:|/|?|=|-]|\\|\'|\^|\|/g;
    return (text.toString() === text.toString().replace(pattern, "")); 
}
export function isValidRuc(ruc) {
    let strRuc = ruc.toString();
    if (strRuc.length != 11) return false;

    let arrVals = ["10", "15", "16", "17", "20"];
    let initStr = strRuc.substring(0, 2);
    if (arrVals.indexOf(initStr) == -1) return false;

    let charsRUC = strRuc.split('').reverse();
    let minFactor = 2, mulFactor = 7, currentFactor = 2, sum = 0;
    for (let i = 1; i < 11; i++) {
        sum += charsRUC[i] * currentFactor;
        if (currentFactor == mulFactor){
            currentFactor = minFactor;
        }else{
            currentFactor++;
        }
    }
    let checkDigit = 11 - (sum % 11);
    switch (checkDigit) {
        case 10:
            checkDigit = 0;
            break;
        case 11:
            checkDigit = 1;
            break;
    }
    return charsRUC[0] == checkDigit;
}