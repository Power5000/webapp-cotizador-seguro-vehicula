import {
    ThemeProvider as MaterialThemeProvider,
    createMuiTheme,
  } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';

import { ThemeChange, ThemeContext, THEMES } from '@contexts/theme.context';
import { KEY_USER_THEME } from '@toolbox/constants/local-storage';
import { useLocalStorage } from '@toolbox/hooks/local-storage.hook';

type ThemeProps = {
    children?: React.ReactNode
}

const RimacTheme : React.FC<ThemeProps> = ({
    children
}) => {

    let materialColor: any = green;
    let toggleTheme = () => {
        setState(state => ({
            ...state,
            theme: JSON.stringify(state.theme) === JSON.stringify(THEMES.dark)
                ? THEMES.light
                : THEMES.dark,
        }));
    };

    let loadThemeStyles = () => {
        const styles: CSSStyleDeclaration = document.documentElement.style;
        for (const key in state.theme) {
            if (Object.prototype.hasOwnProperty.call(state.theme, key)) {
                const element: string = state.theme[key];
                styles.setProperty(key, state.theme[key]);
            }
            if (key === '--color-secondary') {
                const color = state.theme['--color-secondary']
                materialColor = {
                    50: color,
                    100: color,
                    200: color,
                    300: color,
                    400: color,
                    500: color,
                    600: color,
                    700: color,
                    800: color,
                    900: color,
                    A100: color,
                    A200: color,
                    A400: color,
                    A700: color,
                };
            }
        }
    }

    const [state, setState] = useLocalStorage<ThemeChange>(
        KEY_USER_THEME, {
            theme: THEMES.light,
            toggleTheme: toggleTheme
        }
    )

    
    loadThemeStyles();
    const theme = createMuiTheme({ palette: { primary: materialColor } });

    return (
        <ThemeContext.Provider value={state}>
            <MaterialThemeProvider theme={theme}>
                {children}
            </MaterialThemeProvider>
        </ThemeContext.Provider>
    );
}

export { RimacTheme };


const purple = {
    50: '#6F7DFF',
    100: '#6F7DFF',
    200: '#6F7DFF',
    300: '#6F7DFF',
    400: '#6F7DFF',
    500: '#6F7DFF',
    600: '#6F7DFF',
    700: '#6F7DFF',
    800: '#6F7DFF',
    900: '#6F7DFF',
    A100: '#6F7DFF',
    A200: '#6F7DFF',
    A400: '#6F7DFF',
    A700: '#6F7DFF',
};