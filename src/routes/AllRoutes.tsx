import React from 'react';
import { BrowserRouter as Router, Route, Redirect, RouteProps, Switch } from 'react-router-dom';
import { readLocalStorage } from '@toolbox/helpers/local-storage-helper';
import { KEY_USER_DATA } from '@constants/local-storage';
import * as Routes from '@constants/route-map';
import { UserAgreement } from '@models/User';

import { Unauthorized } from '@views/_unautorized';
import { HomeView } from '@views/Home/Home';
import { LoginView } from '@views/Login';
import { GreetingView } from '@views/Greeting';

type Menu = {
    name : string,
    route: string,
    icon : string,
    Page : React.FC<any>,
    subMenu : Menu[]
}

class AllRoutes extends React.Component {

    staticMenu: Menu[] = [];
    state = { menu: this.staticMenu };

	getDinamicRoutes = () : JSX.Element[] => {
		const routeDynamics: JSX.Element[] = [];
		this.state.menu.forEach( (menu, key) => {
			menu.subMenu.forEach( (item: Menu) => {
				routeDynamics.push(
					<Route key={key} exact path={item.route} component={
						(props: RouteProps) => <item.Page {...props} />
					}/>
				)
			})
		});
		return routeDynamics;
	}

	getUserRoute = (
		sucess: (t: UserAgreement) => JSX.Element,
		error : () => JSX.Element
	): JSX.Element => {
		const userData: UserAgreement | null = readLocalStorage(KEY_USER_DATA)
		if (userData)
			return sucess(userData)
		return error()
	}

	render() {
		return (
			<Router>
				<Switch>
					<Route exact path={Routes.ROUTE_HOME} component={LoginView} />
					<Route exact path={Routes.ROUTE_PASS_ONE} component={() => this.getUserRoute(
						(t) => <HomeView userData = {t} stepCurrent={Routes.ROUTE_PASS_ONE} title={"Datos Vehiculos"}/>,
						 () => <Redirect to="/" />
					)} />
					<Route exact path={Routes.ROUTE_PASS_TWO} component={() => this.getUserRoute(
						(t) => {
							if (t.vehicle.brand) {
								return <HomeView userData = {t} stepCurrent={Routes.ROUTE_PASS_TWO} title={"Armar Plan"}/>
							} else {
								return <Redirect to= {Routes.ROUTE_PASS_ONE} />		
							}
						},
						 () => <Redirect to="/" />
					)} />
					<Route exact path={Routes.ROUTE_CONGRATS} component={() => this.getUserRoute(
						(t) => <GreetingView userData = {t} title={"Gracias"}/>,
						 () => <Redirect to="/" />
					)} />
					{
						this.getDinamicRoutes()
					}
					<Route path='*' exact={true} component={() => {
						return <Unauthorized />
					}} />
				</Switch>
			</Router>
		)
	}
}

export default AllRoutes;