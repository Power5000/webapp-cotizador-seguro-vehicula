import React from 'react';
import { Provider } from 'react-redux';
import { RimacTheme } from './themes';
import store from './redux/store';
import AllRoutes from './routes/AllRoutes';

function App() {
    return (
        <Provider store = {store}>
            <RimacTheme>
                <AllRoutes />
            </RimacTheme>
        </Provider>
    );
}

export default App;
