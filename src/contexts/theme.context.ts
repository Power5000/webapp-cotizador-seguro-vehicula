import React from 'react';

// type Getters<Type> = {
    // [key in keyof Type] : string
//     [Property in keyof Type as `--${string & Property}`]: string
// };

export type CSSThemeValues = {
    '--color_bg_menu'     : string,
    '--color_bg_spinner'  : string,
    '--color-main-page'   : string,
    '--color-bg-button'   : string,

    '--shadow-bg-menu'    : string,
    '--gray-normal-color' : string,
    '--gray-soft-color'   : string,
    '--item-menu-project' : string,

    '--item-menu-selected': string,
    '--item-menu-collapse': string,
    '--item-menu-letter'  : string,
    '--item-menu-default' : string,
    '--item-menu-icon'    : string,

    '--color-white'       : string,
    '--color-black'       : string,
    '--color-main'        : string,

    '--color-secondary'   : string,
    '--color-primary'     : string
}

export type ThemeProps = {
    light : CSSThemeValues, dark: CSSThemeValues
}
export type ThemeChange = {
    theme: CSSThemeValues,
    toggleTheme: () => void
}

export const THEMES: ThemeProps = {
    light: {
        "--color_bg_menu": "rgba(0,0,0,0.5)",
        "--color_bg_spinner": "#333",
        "--color-main-page": "#efefef",
        "--color-bg-button": "#efefef",

        "--shadow-bg-menu": "rgba(0,0,0,0.25)",
        "--gray-normal-color": "#8e8e8e",
        "--gray-soft-color": "#F5F4F7",
        "--item-menu-project": "#ccdbe8",

        "--item-menu-selected": "#E0F2EB",
        "--item-menu-collapse": "#F3F1FD",
        "--item-menu-letter": "#17a2b8",
        "--item-menu-default": "#686868",
        "--item-menu-icon": "#313131",

        "--color-white": "#ffffff",
        "--color-black": "black",
        "--color-main": "red",

        "--color-secondary": "#6F7DFF",
        "--color-primary": "#EF3340"
    },
    dark: {
        "--color_bg_menu": "rgba(0,0,0,0.5)",
        "--color_bg_spinner": "#333",
        "--color-main-page": "#222222",
        "--color-bg-button": "#efefef",
    
        "--shadow-bg-menu": "rgba(0,0,0,0.25)",
        "--gray-normal-color": "#8e8e8e",
        "--gray-soft-color": "#F5F4F7",
        "--item-menu-project": "#ccdbe8",
        
        "--item-menu-selected": "#E0F2EB",
        "--item-menu-collapse": "#F3F1FD",
        "--item-menu-letter": "#17a2b8",
        "--item-menu-default": "#686868",
        "--item-menu-icon": "#313131",
    
        "--color-white": "#ffffff",
        "--color-black": "black",
        "--color-main": "black",

        "--color-secondary": "#6F7DFF",
        "--color-primary": "#EF3340"
    }
};

export const ThemeContext = React.createContext<CSSThemeValues|ThemeChange>(THEMES.light);
