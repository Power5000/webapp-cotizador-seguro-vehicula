import { UserAgreement } from "@service/models/User"

export type ModalProps = {
    open: boolean,
    title: string | React.ReactNode,
    description: string,
    confirm: () => void,
    close: () => void
}

type Greeting = {
    user: UserAgreement
}
export type Props = Greeting