import React, { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router';

import { Button } from '@components/common/Button';
import { ConfirmDialog } from "@components/common/DialogConfirm";
import { APP_URL_HOW_USE_SECURE } from '@toolbox/defaults/app';
import { GreetingMdIcon, GreetingXlIcon } from '@constants/icons';

import { ModalProps, Props } from './Greeting.type';

export const Greeting: React.FC<Props> = (
    props
) : JSX.Element => {

    const history    = useHistory();
    const leaveView  = useRef<boolean>(false);
    const [modalData, setDataModal] = useState<ModalProps>({
        title: '',
        open: false,
        description: '',
        confirm: () => {},
        close: () => {},
    });

    useEffect(() => {
        const evt = () => {
            if (leaveView.current) {
                const action = () => {
                    setDataModal({...modalData,open: false});
                    history.replace("/");
                }
                setTimeout(() => {
                    setDataModal({
                        open: true,
                        title: 'FELICIDADES',
                        description: 'Gracias por utilizar el app de cotizador de seguro vehicular :D',
                        confirm: action,
                        close: action,
                    })
                }, 2000);
            }
        }
        
        document.addEventListener("visibilitychange", evt);
        return () => {
            document.removeEventListener("visibilitychange", evt);
        }
    },  // eslint-disable-next-line
    []);

    return (
        <>
            <section className="p-greeting__parent">
                <div className="p-greeting__first-content">
                    <GreetingXlIcon className="p-greeting__xl-icon"/>
                    <GreetingMdIcon className="p-greeting__md-icon"/>
                </div>
                <div className="p-greeting__second-content">
                    <div className="p-greeting__body">
                        <h3 className="p-title__secure --reverse">
                            <span>¡Te damos la bienvenida!</span>
                            <p>Cuenta con nosotros para proteger tu vehículo</p>
                        </h3>

                        <div className="p-greeting__mailer">
                            <span>{"Enviaremos la confirmación de compra de tu Plan Vehícular Tracking a tu correo:"}</span>
                            <div>{props.user.email}</div>
                        </div>
                        <Button
                            type      = "button"
                            text      = {"CÓMO USAR MI SEGURO"}
                            action    = {() => {
                                window.open(APP_URL_HOW_USE_SECURE, '_blank')
                                leaveView.current = true
                            }}
                            className = "contained"
                        />
                    </div>
                </div>
                <ConfirmDialog 
                    open      = {modalData.open}
                    title     = {modalData.title}
                    message   = {modalData.description}
                    onConfirm = {modalData.confirm}
                    onClose   = {modalData.close}
                />
            </section>
        </>
    );
}