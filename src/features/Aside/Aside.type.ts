export type StepsProps = {
    nro: string,
    title: string,
    active: boolean
}
export type AsideProps = {
    nro: '1' | '2'
}