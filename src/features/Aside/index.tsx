import React from 'react';
import { useHistory } from 'react-router';
import { AsideProps, StepsProps } from './Aside.type';
import { ArrowBack } from '@constants/icons';
import { Icon } from '@components/common/Icon';
import { MobileStepper } from '@components/common/MobileStepper';
import { useState } from 'react';
import { useEffect } from 'react';

const Progress: React.FC = () : JSX.Element => {
    return (
        <div className="p_home__progress"></div>
    )
}

const Steps: React.FC<StepsProps> = (
    {nro, title, active}
) : JSX.Element => {
    return (
        <article className="p-home__bubble" aria-disabled={!active}>
            <div>{nro}</div>
            <h3>{title}</h3>
        </article>
    )
}

export const Aside: React.FC<AsideProps> = (
    props
) : JSX.Element => {

    const history  = useHistory();
    const [stepNro, setStepNro] = useState(1)

    useEffect(() => {
        setTimeout(() => {
            setStepNro(props.nro === '1' ? 1 : 4);
        }, 100)
    },// eslint-disable-next-line
    []);

    return (
        <>
            <aside className="p-home__aside">
                <div className="p-home__aside-wizard">
                    <div className="p-home__aside-mobile-step">
                        <button className="p-home__aside-back" onClick={() => props.nro==='1' ? history.replace("/") : history.goBack()}>
                            <Icon Svg={ArrowBack}></Icon>
                        </button>
                        <p>{"PASO "+props.nro+" DE 2"}</p>
                        <MobileStepper
                            variant="progress"
                            steps={5}
                            position="static"
                            activeStep={stepNro}
                            nextButton={<></>}
                            backButton={<></>}
                        />
                    </div>
                    <div className="p-home__aside-desktop-step">
                        <Steps nro={"1"} title={"Datos Auto"} active={props.nro==='1'}/>
                        <Progress />
                        <Steps nro={"2"} title={"Arma tu plan"} active={props.nro==='2'}/>
                    </div>
                </div>
            </aside>
        </>
    );
}