import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { Icon } from '@/components/common/Icon';
import { RimacTitle } from '@components/common/Title';
import { Select } from '@components/common/Select';
import { Button } from '@components/common/Button';
import { Radio } from '@components/common/Checkbox';
import { Cotizate } from '@components/common/Cotizate';

import { ROUTE_CONGRATS, ROUTE_PASS_TWO } from '@toolbox/constants/route-map';
import { ArrowBack, CarIcon } from '@toolbox/constants/icons';
import { KEY_USER_DATA } from '@toolbox/constants/local-storage';
import { APP_URL_MORE_MODELS } from '@toolbox/defaults/app';
import { readLocalStorage, saveLocalStorage } from '@/toolbox/helpers/local-storage-helper';
import { INC_COTIZE, MAX_COTIZE, MIN_COTIZE, SHOW_YEARS } from '@toolbox/defaults/app';

import { brandService } from '@service/services/brand.service';
import { BrandAgreement } from '@service/models/Brand';
import { UserAgreement } from '@service/models/User';

import { Props } from './VehicleData.type';

const year     = (new Date).getFullYear()-SHOW_YEARS+1;
const dataYear = Array(SHOW_YEARS).fill('.').map((i,k) => ({
    name: year+k+'',
    value: year+k+''
}));


const AsideVehicleData = () : JSX.Element => (
    <aside className="p-home__micelanius">
        <h3>AYUDA</h3>
        <div className="p-home__line"></div>
        <div className="p-home__model">
            <div className="p-home__model-text">
                <span>¿No encuentras el modelo?</span>
                <button className="p-home__text-button">
                    <a href={APP_URL_MORE_MODELS} target="_blanck">CLICK AQUI</a>
                </button>
            </div>
            <div className="p-home__model-icon">
                <Icon Svg={CarIcon} />
            </div>
        </div>
    </aside>
)

export const VehicleData: React.FC<Props> = (props: Props) : JSX.Element => {

    const history = useHistory();
    const [check, changeCheck] = useState<any>('No');
    const [brands, setBrands]  = useState<BrandAgreement[]>([]);

    const [dataForm, setData]  = useState({
        selectBrand : { name: 'Wolskwagen', value: 'Wolskwagen' },
        selectYear  : { name: 2019, value: 2019 },
        cotizate    : MIN_COTIZE
    });
    
    const userData: UserAgreement | null = readLocalStorage(KEY_USER_DATA)
    
    const minMount  = useRef(MIN_COTIZE);
    const maxMount  = useRef(MAX_COTIZE);
    const increment = useRef(INC_COTIZE);

    const handleDataForm = (id, option) => {
        setData({...dataForm, [id] : option});
    }

    const handleSaveData = () => {
        const user = userData || {
            name: '', email: '', phone: '', user_id: '', username: '', vehicle: {
                brand: 'aa', name: 'aa', plateNumber: 'aa', year: 'aa'
            }
        };
        user.vehicle.brand = dataForm.selectBrand.value;
        user.vehicle.year  = dataForm.selectYear.value+'';

        saveLocalStorage(KEY_USER_DATA, user);
        setTimeout(() => {
            history.push(ROUTE_PASS_TWO);
        }, 0);
    }

    useEffect(() => {
        brandService.getBrands().then(setBrands)
    }, // eslint-disable-next-line
    [])

    return (
        <>
            <section className="p-home__vehicle-data">
                <div className="p-home__form">
                    <div className="">
                        <button className="p-home__back-step" onClick={() => history.replace("/")}>
                            <Icon className="p-home__aside-back" Svg={ArrowBack}></Icon>
                            <span>VOLVER</span>
                        </button>
                    </div>
                    
                    <RimacTitle text={"!Hola, "} main={props.user.name+'!'}/>

                    <p className="p-home__complete-text">Completa los datos de tu auto</p>
                    <section className="p-home__select-vehicle">
                        <div className="p-home__selects">
                            <Select
                                id             = {"selectYear"}
                                variant        = {'outlined'}
                                label          = {'Año'}
                                options        = {dataYear}
                                getOptionLabel = {(option) => option.name+''}
                                error          = {false}
                                onChange       = {(e, option) => handleDataForm('selectYear', option)}
                                value          = {dataForm.selectYear}
                                style          = {{ width : '100%', marginRight: -1 }}
                                helperText     = {''}
                                disableClearable = {true}
                            />
                            <Select
                                id             = {"selectBrand"}
                                variant        = {'outlined'}
                                label          = {'Marca'}
                                options        = {brands}
                                getOptionLabel = {(option) => option.name+''}
                                error          = {false}
                                value          = {dataForm.selectBrand}
                                onChange       = {(e, option) => handleDataForm('selectBrand', option)}
                                style          = {{ width : '100%', marginRight: -1 }}
                                helperText     = {''}
                                disableClearable = {true}
                            />
                        </div>

                        <AsideVehicleData />

                    </section>
                    <div className="p-home__ask-gas">
                        <label>¿Tu auto es a gas?</label>
                        <div>
                            <Radio
                                label = "Si"
                                value = "Si"
                                checkName = "checkCar"
                                checked={check}
                                onChangeCheck={(data) => {
                                    changeCheck(data)
                                }}
                            />
                            <Radio
                                label = "No"
                                value = "No"
                                checkName = "checkCar"
                                checked={check}
                                onChangeCheck = {(data) => {
                                    changeCheck(data)
                                }}
                            />
                        </div>
                    </div>
                    <div className="p-home__line"></div>
                    <section className="p-home__amount-sum">
                        <div className="p-home__amount-desc">
                            <div className="p-home__amount-line">
                                <h3>Indica la suma asegurada</h3>
                                <span>MIN {minMount.current} </span>
                                <span className="p-home__amount-line-bar">|</span>
                                <span>MAX {maxMount.current} </span>
                            </div>
                            <div className="p-home__amount-action">
                                <Cotizate count={minMount.current} max={maxMount.current} min={minMount.current} increment={increment.current} simbol={'$'}/>
                            </div>
                        </div>
                        <div className="p-home__amount-button">
                            <Button text="CONTINUAR" type="button" action={() => handleSaveData()} className="contained"/>
                        </div>
                    </section>
                </div>
            </section>
        </>
    );
}