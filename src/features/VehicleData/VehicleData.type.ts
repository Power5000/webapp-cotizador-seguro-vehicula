import { UserAgreement } from "@/service/models/User"

type VehicleData = {
    user: UserAgreement
}
export type Props = VehicleData