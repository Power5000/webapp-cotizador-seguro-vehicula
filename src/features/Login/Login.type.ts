import React from "react";
import { NestedValue } from "react-hook-form";

export type ModalProps = {
    open: boolean,
    title: string | React.ReactNode,
    description: string,
}

export type DocumentOption = {
    name: string,
    value: string
}

export type FormValues = {
    seleTypeDoc: NestedValue<DocumentOption[]>,
    textNroDoc: string,
    textPhone: string,
    textPlaca: string,
}