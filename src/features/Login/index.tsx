import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';

import { InputText } from '@components/common/InputText';
import { RimacTitle } from '@components/common/Title';
import { Select } from '@components/common/Select';
import { Checkbox } from '@components/common/Checkbox';
import { Button } from '@components/common/Button';
import { Loading } from '@components/common/Loading';
import { ConfirmDialog } from "@components/common/DialogConfirm";

import { BackgroundLogin, LogoXlIcon, LogoMdIcon } from '@constants/icons';
import { ROUTE_PASS_ONE } from '@constants/route-map';
import { APP_URL_TERMS_CONDITIONS, APP_URL_POLITICE } from "@defaults/app";
import { KEY_USER_DATA } from '@constants/local-storage';
import { UserAgreement } from '@models/User';

import { userService } from "@service/services/User.service";
import { useLocalStorage } from "@toolbox/hooks/local-storage.hook";

import { DocumentOption, FormValues, ModalProps } from './Login.type';
import { isDNI, isValidRuc } from "@/toolbox/helpers/validator.helper";

const docDNI = 'DNI';
const docRUC = 'RUC';
const typeDoc: DocumentOption[] = [
    { name: docDNI, value: docDNI },
    { name: docRUC, value: docRUC },
];

export const LoginFeature: React.FC = () : JSX.Element => {
    
    const [loadData, setLoadData]     = useState<boolean>(false);
    const [accept, changeCheck]       = useState<boolean>(true);
    const [userData, setUserData]     = useLocalStorage<UserAgreement|null>(KEY_USER_DATA, null);
    const [modalData, setDataModal]   = useState<ModalProps>({
        title: '',
        open: false,
        description: ''
    });

    const {
        getValues, setValue, register,
        handleSubmit, formState: { errors }
    } = useForm<FormValues>({
        defaultValues: {
            seleTypeDoc : typeDoc,
            textNroDoc  : '',
            textPhone   : '',
            textPlaca   : ''
        }
    });
    const history  = useHistory();
    const onSubmit = handleSubmit(async (data) => {
        if (accept) {
            setLoadData(true);
            const user = await userService.getUser(data.textPhone,data.textPlaca, data.textPlaca);
            setLoadData(false);
            if (user === null) {
                setDataModal({
                    open: true,
                    title: 'ALERTA',
                    description: 'Usuario no encontrado, escriba correctamente sus datos.'
                })
            } else {
                setUserData(user);
                history.replace(ROUTE_PASS_ONE, user)
            }
        } else {
            setDataModal({
                open: true,
                title: 'AVISO',
                description: 'Para continuar, acepte la política de privacidad.'
            })
        }

    });

    React.useEffect(() => {
        register("seleTypeDoc", {
            validate: (value) => {
                return !!value || "Requerido."
            }
        });
        register("textNroDoc", {
            validate: (value) => {
                const typeDoc: any = getValues('seleTypeDoc');
                if (!!(value || '') ) {
                    if (!!typeDoc) {
                        switch (typeDoc.name) {
                            case docDNI: return (isDNI((value || ''))) || 'DNI inválido'
                            case docRUC: return (isValidRuc((value || ''))) || 'RUC inválido'
                        }
                    } else return true
                } else return "Campo requerido."
            },
        });
        register("textPhone", {
            validate: (value) => !!(value || '').length || "Campo requerido."
        });
        register("textPlaca", {
            validate: (value) => !!(value || '').length || "Campo requerido."
        });
    }, // eslint-disable-next-line
    [register]);

    React.useEffect(() => {
        setUserData(null);
    }, // eslint-disable-next-line
    []);

    return (
        <>
            <form className="p-login__content-page"  onSubmit={onSubmit}>
                {loadData && <Loading/>}
                <section className="p-login__first">
                    <div className="p-login__content-background">
                        <BackgroundLogin className="p-login__svg-background" />
                    </div>
                    <div className="p-login__data-background">
                        <LogoXlIcon className="p-login__svg-car"/>
                        <LogoMdIcon className="p-login__svg-car --mobile"/>
                        <div className="p-login__new-car">
                            {"¡NUEVO!"}
                        </div>

                        <RimacTitle text={"Seguro "} main={"Vehicular Tracking"}/>

                        <div className="p-login__paragraph">
                            {"Cuentanos donde le haras seguimiento a tu seguro"}
                        </div>
                    </div>
                </section>
                <section className="p-login__form">
                    <div className="p-login__content-form">
                        <h2 className="p-login__form-title">
                            {"Déjanos tus datos"}
                        </h2>
                        <div className="p-login__form-login">
                            <div className="p-login__form-input --inline">
                                <Select
                                    variant        = {'outlined'}
                                    id             = {"text"}
                                    placeholder    = {'TIPO'}
                                    options        = {typeDoc}
                                    getOptionLabel = {(option:DocumentOption) => option.name}
                                    error          = {Boolean(errors?.seleTypeDoc)}
                                    onChange       = {(e, options) => setValue("seleTypeDoc", options)}
                                    style          = {{ width : '40%', marginRight: -1 }}
                                    helperText     = {errors?.seleTypeDoc?.message}
                                />
                                <InputText
                                    id         = "textNroDoc"
                                    label      = "Nro. de doc"
                                    type       = "number"
                                    variant    = "outlined"
                                    className  = "p-login__input --right"
                                    error      = {Boolean(errors?.textNroDoc)}
                                    onChange   = {(e) => setValue("textNroDoc", e.target.value)}
                                    helperText = {errors?.textNroDoc?.message}
                                />
                            </div>
                            <div className="p-login__form-input">
                                <InputText
                                    id         = "textPhone"
                                    label      = "Celular"
                                    variant    = "outlined"
                                    className  = "p-login__input"
                                    error      = {Boolean(errors?.textPhone)}
                                    onChange   = {(e) => setValue("textPhone", e.target.value)}
                                    helperText = {errors?.textPhone?.message}
                                />
                            </div>
                            <div className="p-login__form-input">
                                <InputText
                                    id         = "textPlaca"
                                    label      = "Placa"
                                    variant    = "outlined"
                                    className  = "p-login__input"
                                    error      = {Boolean(errors?.textPlaca)}
                                    onChange   = {(e) => setValue("textPlaca", e.target.value)}
                                    helperText = {errors?.textPlaca?.message}
                                />
                            </div>
                            <div className="p-login__politice">
                                <Checkbox checkName="checkPolitice" onChangeCheck={(data) => {
                                    changeCheck(data.checkPolitice)
                                }} value={accept} />
                                <span>Acepto la <a href={APP_URL_TERMS_CONDITIONS} target="_blanck">{"Política de Protección de Datos Personales"}</a> y los <a href={APP_URL_POLITICE} target="_blanck">{"Términos y Condiciones."}</a></span>
                            </div>
                            <Button text="COTÍZALO" type="submit"  action={() => {}} className="contained"/>
                        </div>
                    </div>
                </section>
                <ConfirmDialog 
                    open      = {modalData.open}
                    title     = {modalData.title}
                    message   = {modalData.description}
                    onConfirm = {() => setDataModal({...modalData,open: false})}
                    onClose   = {() => setDataModal({...modalData,open: false})}
                />
            </form>
        </>
    );
}

