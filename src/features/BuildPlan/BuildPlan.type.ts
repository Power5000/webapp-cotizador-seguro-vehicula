import { SVGAttributes } from "react"
import { UserAgreement } from "@/service/models/User"

type VehicleData = {
    user: UserAgreement
}
export type ArticleClanProps = {
    Icon: React.FC<SVGAttributes<any>>,
    desc: string,
    title: string,
    name: string,
    checked: boolean,
    expand: boolean,
    changeCheck: (data: any) => void
}
export type Props = VehicleData