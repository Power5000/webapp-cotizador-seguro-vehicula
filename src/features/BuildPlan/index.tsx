import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { Icon } from '@components/common/Icon';
import { RimacTitle } from '@components/common/Title';
import { Button } from '@components/common/Button';
import Tabs from '@components/common/Tabs';
import { Switch } from '@components/common/Switch';
import { Loading } from '@components/common/Loading';
import { ArrowLess, ArrowMore, PersonBuildPlan, Choque, LLantaRobada, PerdidaTotal } from '@constants/icons';

import { ROUTE_CONGRATS } from '@toolbox/constants/route-map';
import { ArrowBack, TrofeoIcon, CheckIcon } from '@toolbox/constants/icons';

import { ArticleClanProps, Props } from './BuildPlan.type';

const AsideBuildPlan = ({ action,amounts }) : JSX.Element => (
    <aside className="p-home__micelanius">
        <div className="p-home__amount-aside">
            <div>
                <p className="p-home__base-amount">$ {amounts}.00</p>
                <span className="p-home__base-mount">mensuales</span>
            </div>
            <TrofeoIcon />
        </div>
        <div className="p-home__line"></div>
        <div className="p-home__amount-checks">
            <div className="p-home__model-text">
                <span className="--title">El precio incluye:</span>
                <span> <CheckIcon className="p-home__amount-check" /> Llanta de repuesto</span>
                <span> <CheckIcon className="p-home__amount-check" /> Análisis de motor</span>
                <span> <CheckIcon className="p-home__amount-check" /> Aros gratis</span>
                <Button style={{minWidth: 200, marginTop: 32}} text="LO QUIERO" type="button" action={action} className="contained"/>
            </div>
        </div>
    </aside>
)

const ArticlePlan: React.FC<ArticleClanProps> = ({
    Icon, desc, title, name, checked, expand, changeCheck
}) : JSX.Element => {

    const [expandMore, setExpand] = useState<boolean>(expand);

    return (
        <article className={"p-home__arcicle-plan " + (expandMore ? "--minimize": "")}>
            <div>
                <Icon></Icon>
            </div>
            <div className="p-home__article-item">
                <div className="p-home__aricle-plan-title">
                    <h3>{title}</h3>
                    <div><Switch name={name} checked={checked} onChange={changeCheck}/></div>
                </div>
                <div className="p-home__aricle-plan-description">
                    {desc}
                </div>
                <button onClick={() => setExpand(!expandMore)}>
                    {!expandMore ? (
                        <>
                            Ver menos
                            <ArrowLess />
                        </>
                    ) : (
                        <>
                            Ver más
                            <ArrowMore />
                        </>
                    )}
                </button>
            </div>
        </article>
    )
}

export const BuildPlan: React.FC<Props> = ({
    user
}) : JSX.Element => {

    const history = useHistory();
    const baseAmount = 20;
    const [loadData, setLoadData]     = useState<boolean>(false);
    const [amounts, changeBaseAmount] = useState(baseAmount);
    const [checked, changeCheckeds] = useState({});

    const rules = {
        "llanta-robada": 15,
        "robo-parcial": 20,
        "perdida-total": 50
    }

    const handleChangeAmount = (data) => {
        const raw = {...checked, ...data};
        let amount = amounts;
        Object.keys(data).map((key) => {
            let status = raw[key];
            if (!!status) {
                amount += rules[key];
            } else {
                amount -= rules[key];
            }
        })
        changeBaseAmount(amount);
        changeCheckeds(raw);
    }

    const handleGeneratePlan = () => {
        setLoadData(true);
        setTimeout(() => {
            setLoadData(false);
            history.push(ROUTE_CONGRATS);
        }, 1000);
    }
    
    return (
        <>
            <section className="p-home__build-plan">
                <div className="p-home__form">
                    <div className="">
                        <button className="p-home__back-step" onClick={() => history.goBack()}>
                            <Icon className="p-home__aside-back" Svg={ArrowBack}></Icon>
                            <span>VOLVER</span>
                        </button>
                    </div>
                    
                    <RimacTitle text={"Mira las coberturas"} main={''}/>
                    <p className="p-home__complete-text">Conoce las coberturas para tu plan</p>

                    <section className="p-home__select-vehicle">
                        <div className="p-home__chips">
                            <div className="p-home__chips-panel">
                                <h4>Placa: {user.vehicle.plateNumber}</h4>
                                <div className="p-home__chip-brand">{user.vehicle.brand} {user.vehicle.year}</div>
                                <button>Editar</button>
                            </div>
                            <div className="p-home__build-person">
                                <PersonBuildPlan/>
                            </div>
                        </div>
                        <AsideBuildPlan action={handleGeneratePlan} amounts={amounts}/>
                    </section>

                </div>

                <div style={{width: '100%', backgroundColor: 'white'}}>
                    <div className="p-home__tabs">
                        <h3>Agrega o quita las coberturas</h3>
                        <Tabs
                            tabLabelOne = "Protege a tu auto"
                            tabLabelTwo   = "Protege a los que te rodea"
                            tabLabelThree = "mejora tu plan"
                            PanelOne = {
                                <div>
                                    <ArticlePlan
                                        Icon={LLantaRobada}
                                        checked={checked["llanta-robada"]}
                                        expand={false}
                                        name="llanta-robada"
                                        changeCheck={handleChangeAmount}
                                        title={"Llanta robada"}
                                        desc={"He salido de casa a las cuatro menos cinco para ir a la academia de ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na llego a la academia que está en el centro del pueblo en una plaza medio-grande y dejo donde siempre la bici atada con una pitón a un sitio de esos de poner las bicis"}
                                    />
                                    <ArticlePlan
                                        Icon={Choque}
                                        checked={checked["robo-parcial"]}
                                        name="robo-parcial"
                                        changeCheck={handleChangeAmount}
                                        expand={true}
                                        title={"Choque y/o pasarte la luz roja"}
                                        desc={"He salido de casa a las cuatro menos cinco para ir a la academia de ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na llego a la academia que está en el centro del pueblo en una plaza medio-grande y dejo donde siempre la bici atada con una pitón a un sitio de esos de poner las bicis"}
                                    />
                                    <ArticlePlan
                                        Icon={PerdidaTotal}
                                        checked={checked["perdida-total"]}
                                        name="perdida-total"
                                        changeCheck={handleChangeAmount}
                                        expand={true}
                                        title={"Atropello en la vía Evitamiento"}
                                        desc={"He salido de casa a las cuatro menos cinco para ir a la academia de ingles de mi pueblo (Sant Cugat, al lado de Barcelona) con mi bici, na llego a la academia que está en el centro del pueblo en una plaza medio-grande y dejo donde siempre la bici atada con una pitón a un sitio de esos de poner las bicis"}
                                    />
                                </div>
                            }
                            PanelTwo = {
                                <div className="p-home__aricle-plan-title">
                                    <h3>No hay planes</h3>
                                </div>}
                            PanelThree = {
                                <div className="p-home__aricle-plan-title">
                                    <h3>No hay planes</h3>
                                </div>
                            }
                        />
                    </div>
                </div>

            </section>
            <footer className={"c-footer__content p_home__footer p-home__footer-amount"} style={{zIndex:222}}>
                <div>
                   <p className="p-home__base-amount">$ {amounts}.00</p>
                   <span className="p-home__base-mount">MENSUAL</span>
                </div>
                <Button text="LO QUIERO" type="button" action={handleGeneratePlan} className="contained"/>
            </footer>
            {loadData && <Loading title={"Generando plan ..."}/>}
        </>
    );
}