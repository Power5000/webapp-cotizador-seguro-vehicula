# Cotizador de Seguro Vehicular 

_Proyecto dedicado para conocer los planes vehiculares y las cotizaciones_

## Comenzando 🚀

_Estas instrucciones te permitirán desplegar la aplicación de forma local y en productivo a través de CI/CL Gitlab._

### Pre-requisitos 📋

* [node v14.17.1](https://nodejs.org/dist/v14.17.1/node-v14.17.1-x64.msi) - Gestor de paquetes de Javascript
* [git](https://github.com/git-for-windows/git/releases/download/v2.32.0.windows.1/Git-2.32.0-64-bit.exe) - Versionado

### Instalación (LOCAL)🔧

_Seguiremos la instalación paso a paso_

```
git clone https://gitlab.com/rimac3/webapp-cotizador-seguro-vehicular.git
git checkout pmendoza
npm install -g yarn
yarn install
```

_Despliegue http://localhost:3000_

```
yarn start
```
### Instalación (PRD)🔧

_los pasos están detallados en .gitlab-ci.yml_

```
yarn install
yarn build
firebase use webapp-rimac-csv --token $FIREBASE_TOKEN
firebase deploy --only hosting -m "Pipe $CI_PIPELINE_ID Build $CI_BUILD_ID" --token $FIREBASE_TOKEN
```

## Librerías y/o Frameworks 🛠️

* [Material UI](https://material-ui.com/) - El framework de Componentes
* [crypto-js](https://www.npmjs.com/package/crypto-js) - Encriptador de datos
* [node-sass](https://www.npmjs.com/package/node-sass) - Styles SASS
* [react-helmet](https://www.npmjs.com/package/react-helmet) - Para actualizar la cabecera de HTML
* [react-redux](https://www.npmjs.com/package/react-redux) - Redux

## Autor ✒️

* **Pedro Mendoza Ardiles** - *Trabajo Inicial* - [pmendoza](https://github.com/villanuevand)

## Estructura 📄

```
project-app
src
__test__
    user-detail.spec.js
    dashboard.spec.js
assets
    img
        profile.png
    svg
        product.svg
        edit.svg
theme
    app.theme.js
contexts
    user.context.js
components
    common
        Comment
            __test__
                comment.test.js
            Comment.js
            Comment.css
            index.js
        Dropzone
            __test__
            Dropzone.js
            index.js
    sections
        Panel
            __test__
                panel.test.js
            Panel.css
            Panel.js
            index.js
    layout
        Intranet
            __test__
            index.js
    hocs (Hight Order Components)
        with-sidebar.js
views
    __test__
        __snapshots__
        Dashboard.spec.js
    Dashboard
        __test__
            dashboard.test.js
        components
        Dashboard.css
        Dashboard.js
        index.js
        Statistics
            Statistics.js
            StatisticsView.js
            index.js
    UserDetail
        __test__
            UserDetail.test.js
        UserDetail.js
        UserDetail.css
        UserDetail.type.js
        UserDetail.job.js
        index.js
jobs
    __test__
        loader.spec.js
        product.spect.js
        user.spec.js
    loadder
        loadder.job.js
        product.job.js
        user.job.js
toolbox
    constants
        app.ts
        input-type.ts
        pages.ts
        services.ts
        action-type.js
    types
        user.type.ts
        product.type.ts
    defaults
        user.ts
        product.ts
        country.ts
    hooks
        user.hook.ts
        page.hook.ts
        custom.hooks.ts
    helpers
        validators.helper.ts
        object.helper.ts
    css
        reset.css
        color.css
providers
    storage
        index.js
    modules
        user.module.js
        product.module.js
services
    http
        __test__
            user.service.spec.js
            product.service.spec.js
            country.service.spec.js
        user.service.ts
        product.service.js
        country.service.js
        http.js
redux
    actions
        auth.action.js
        menu.action.js
        user.action.js
        index.js
    dispatchers
        index.js
    getters
        index.js
    reducers
        menu.reducer.js
        user.reducer.js
        auth.reducer.js
        index.js
    store.js


---
```

⌨️ con ❤️ por [pmendoza](https://gitlab.com/Power5000/webapp-cotizador-seguro-vehicula) 😊